/* class Figura {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

class Rectangulo extends Figura {
  constructor(x, y, lado1, lado2) {
    super(x, y);
    this.lado1 = lado1;
    this.lado2 = lado2;
  }
  area() {
    return this.lado1 * this.lado2;
  }
}

class Triangulo extends Figura {
  constructor(x, y, base, altura) {
    super(x, y);
    this.base = base;
    this.altura = altura;
  }
  area() {
    return (this.base * this.altura) / 2;
  }
}

class Cuadrado extends Rectangulo {
  constructor(x, y, medida) {
    super(x, y, medida, medida);
  }
}

a = new Rectangulo(1, 2, 2, 4);
console.log(`La superficie del rectangulo es: ${a.area()}`);
b = new Triangulo(1, 2, 2, 4);
console.log(`La area del trinagulo es: ${b.area()}`);
c = new Cuadrado(3);
console.log(`La area del cuadrado es: ${a.area()}`); */

/* let letras = ["a", "b", "c"];
let nums = [1, 2, 3];
let mix = [letras, nums];
console.log(mix); */

/* //Destructuring con arrays

let nums = [1, 2, 3, 4, 5];
[a, b, , d, e] = nums;
console.log(a, b, d, e);
//Destructuring con objectos

let pais = [
  { id: 1, name: "Nepal" },
  { id: 2, name: "Spain" },
];
[a, b] = pais;
console.log(a.name, b.name); */
