const API_URL = "http://localhost:3000/api/cp_pais/";
const paginas = 20;
var numero = 0;

function cargaDatos() {
    get(API_URL + "?_p=" + numero)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });
}
get(API_URL + "count")
    .then(function (response) {
        paginacion(JSON.parse(response));
    })
    .catch(function (error) {
        console.error(error);
    });

cargaDatos();



function presentaLista(respuesta) {



    tbody = document.createElement("tbody");
    tbody.setAttribute("id", "tablaBody");

    table = document.getElementById("table");
    table.appendChild(tbody);
    
    respuesta.forEach(row => {
        tr = document.createElement("tr");
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.country_id));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.iso2));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.short_name));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.spanish_name));
        tr.appendChild(td);
        td = document.createElement("td");
        td.appendChild(document.createTextNode(row.numcode));
        tr.appendChild(td);
        td = document.createElement("td");
        btn = document.createElement('button');
        btn.setAttribute("class", "btn btn-dark");
        td.appendChild(btn);
        btn.appendChild(document.createTextNode("Edit"));
        btn.setAttribute("onclick", "editar('" + row.country_id + "')");
        tr.appendChild(td);
        td = document.createElement("td");
        btn = document.createElement('button');
        btn.setAttribute("class", "btn btn-danger");
        td.appendChild(btn);
        btn.appendChild(document.createTextNode("Delete"));
        btn.setAttribute("onclick", "borrar('" + row.country_id + "')");
        tr.appendChild(td);

        tbody.appendChild(tr);

    })

}


function paginacion(respuesta) {
    pa = document.getElementById("paginas");
    ppa = document.createElement("div");

    var registros = respuesta[0].no_of_rows;


    cant = registros / paginas;
    for (i = 1; i < cant; i++) {

        enlace = document.createElement("a");
        enlace.setAttribute("href", "#");
        enlace.setAttribute("onclick", "irAPagina('" + i + "')");
        enlace.appendChild(document.createTextNode(i));
        ppa.appendChild(enlace);

    }
    pa.appendChild(ppa);
}

function borrarTabla(){
    var div = document.getElementById("tablaBody");
    var eliminar = div.parentNode;
    eliminar.removeChild(div);

}


function irAPagina(num) {
    numero = num;
    console.log("adentro: " + numero);

    borrarTabla();

    get(API_URL + "?_p=" + numero)
        .then(function (response) {
            presentaLista(JSON.parse(response));
        })
        .catch(function (error) {
            console.error(error);
        });



}

function borrar(id) {


    get(API_URL + id, 'DELETE')
        .then(function () {
           
            borrarTabla();  
            console.log("antes");
            cargaDatos();
            console.log("despues");
            
        })
        .catch(function (error) {
            console.error(error);
        });
}

function editar(id) {
    console.log(id);

    get(API_URL + id)
        .then(function (datos) {
       
        editarTr(datos);  
            
        })
        .catch(function (error) {
            console.error(error);
        });



}

function editarTr(datos){
    
    



}



