class MotorBike {
  constructor(marca, km, cc, price) {
    this.brand = marca;
    this.km = km;
    this.cc = cc;
    this.price = price;
  }
}

let motorBikeList = [
  new MotorBike("Honda", 68000, 249, 5500),
  new MotorBike("Ducati", 128657, 1200, 3400),
  new MotorBike("BMW", 30245, 150, 9500),
  new MotorBike("Suzuki", 89000, 249, 1500),
  new MotorBike("Yamaha", 5000, 125, 7500),
  new MotorBike("Vespa", 37000, 500, 500),
  new MotorBike("KTM", 6500, 200, 1000),
  new MotorBike("Kawasaki", 62800, 290, 1000),
  new MotorBike("Bajaj", 23000, 1330, 11100),
  new MotorBike("Kymco", 29000, 250, 1000),
  new MotorBike("PEUGEOT", 62800, 1100, 1000),
  new MotorBike("Peugeot", 24000, 800, 2100),
];
/* console.log(listaMoto[3].price); */

//--------------------------------Sorting the motorcycles depending upon the price--------------------------------//
function sortPrice(a, b) {
  if (a.price < b.price) {
    return -1;
  }
  if (a.price > b.price) {
    return 1;
  }
  return 0;
}

let motorBikeListSorted = [...motorBikeList]; //<--- Destructuración para no manipular directamente el array original

motorBikeListSorted = motorBikeListSorted.sort(sortPrice);

console.log(
  `El moto más barata es la de ${motorBikeListSorted[0].brand} y vale ${motorBikeListSorted[0].price}.`
);
console.log(
  `El moto más cara es la de ${
    motorBikeListSorted[motorBikeListSorted.length - 1].brand
  } y vale ${motorBikeListSorted[motorBikeListSorted.length - 1].price}.`
);

//--------------------------------Finding bikes that have less than 30000 km on them--------------------------------//

let motoBikeLessThan30000 = [...motorBikeList];

motoBikeLessThan30000 = motoBikeLessThan30000.filter((el) => el.km < 30000);

let motoBikeNamesLessThan30000 = new Array(); //<-- Aqui irán los nombres de las motos que cumplan km<30000

function motoNamesMenos30K() {
  for (var x = 0; x < motoBikeLessThan30000.length; x++) {
    motoBikeNamesLessThan30000.push(motoBikeLessThan30000[x].brand);
  }
}
motoNamesMenos30K();

console.log(
  `Hay ${
    motoBikeLessThan30000.length
  } motos que tienen menos de 30000km y son de marca ${motoBikeNamesLessThan30000.join(
    ", "
  )}.`
);

//--------------------------------Finding bikes that have less than 30000 km & more than 240cc on them--------------------------------//

let motoList_30k_240 = [...motorBikeList];
let motoList_30k_240_Name = new Array();

motoList_30k_240 = motoList_30k_240.filter(
  (el) => el.km < 30000 && el.cc > 240
);

function list30k240() {
  for (var x = 0; x < motoList_30k_240.length; x++) {
    motoList_30k_240_Name.push(motoList_30k_240[x].brand);
  }
}
list30k240();

console.log(
  `Hay ${
    motoList_30k_240.length
  } motos que tienen menos de 30000km y mas de 240cc y son de marca ${motoList_30k_240_Name.join(
    ", "
  )}`
);

//--------------------------------Finding bikes that have less than 25000 km,more than 350cc & in between 1800-2200 price--------------------------------//

let motoList_25k_350_22k = [...motorBikeList];

motoList_25k_350_22k = motoList_25k_350_22k.filter(
  (el) => el.km < 25000 && el.cc > 350 && 1800 < el.price && el.price < 2200
);

console.log(
  `Hay ${motoList_25k_350_22k.length} motos que tienen menos de 25000km y mas de 350cc y entre 1800€ y 2200€`
);
