
    var url = "paises.json";
    function listaPaises() {
            $.getJSON(url, function (data) {
            
           
    
            for (pais of data.countries){
    
                var tbl = document.createElement('table');
                var tby = document.createElement('tbody')
                var tr = document.createElement('tr');
                var td = document.createElement('td');
    
                td.appendChild(document.createTextNode(pais.id));
                tr.appendChild(td);
                var td = document.createElement('td');
                td.appendChild(document.createTextNode(pais.name));
                tr.appendChild(td);
                tby.appendChild(tr);
                tbl.appendChild(tby);
    
                
    
                $("div.salida").append(tbl);
           
            }
        });
    
    
    };
    
    
    function listarPorId(){
    
        $.getJSON(url, function (data){
    
           var lista = data.countries;
           lista.sort(comparaId);
           
           for (pais of data.countries){
    
            var tbl = document.createElement('table');
            var tby = document.createElement('tbody')
            var tr = document.createElement('tr');
            var td = document.createElement('td');
    
            td.appendChild(document.createTextNode(pais.id));
            tr.appendChild(td);
            var td = document.createElement('td');
            td.appendChild(document.createTextNode(pais.name));
            tr.appendChild(td);
            tby.appendChild(tr);
            tbl.appendChild(tby);
    
    
    
            $("div.salida").append(tbl);
    
            }
            
    
        });
    }
    
    function cienPrimeros(){
        $.getJSON(url, function (data){
            
            
            var lista = data.countries;
             lista.sort(comparaId);
            var lista = lista.filter((x)=> x.id < 101)
        
            for (pais of data.countries){
    
            var tbl = document.createElement('table');
            var tby = document.createElement('tbody')
            var tr = document.createElement('tr');
            var td = document.createElement('td');
    
            td.appendChild(document.createTextNode(pais.id));
            tr.appendChild(td);
            var td = document.createElement('td');
            td.appendChild(document.createTextNode(pais.name));
            tr.appendChild(td);
            tby.appendChild(tr);
            tbl.appendChild(tby);
    
    
    
            $("div.salida").append(tbl);
    
            }
                    
    
        })
    }
    
    
    function empezarPorA(){
        $.getJSON(url, function (data){
        
            var lista = data.countries;
             lista.sort(comparaId);
             lista = lista.filter((x) => x.name.charAt(0) == "A");
        
            
           for (pais of data.countries){
    
            var tbl = document.createElement('table');
            var tby = document.createElement('tbody')
            var tr = document.createElement('tr');
            var td = document.createElement('td');
    
            td.appendChild(document.createTextNode(pais.id));
            tr.appendChild(td);
            var td = document.createElement('td');
            td.appendChild(document.createTextNode(pais.name));
            tr.appendChild(td);
            tby.appendChild(tr);
            tbl.appendChild(tby);
    
    
    
            $("div.salida").append(tbl);
    
            }
    
        })
        
    }
    
    function comparaId(a, b) {
            if (a.id === b.id) return 0;
            if (a.id > b.id) return 1;
            return -1;
          }
    
    